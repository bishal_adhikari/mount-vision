<?php
require "connect.inc.php";


?>
<html lang="en">

<?php include('header.php'); ?>

<body style="background: #f1f1f1">

<?php include('navigation.php'); ?>
<link rel="stylesheet" href="assets/booking/css/booking.css">


<div class="content">
    <main role="main">

        <!-- Main jumbotron for a primary marketing message or call to action -->


        <div class="text-center"
             style="background-image:url(assets/images/slide.jpg); background-size: cover;background-repeat: no-repeat; height:500px;margin-bottom:10px;width: 100%;opacity: 0.8">
        </div>


        <div class="container">
            <!-- Example row of columns -->
            <div class="row">

                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-heading text-center">
                            <h4> Contact Info</h4>


                        </div>
                        <div class="panel-body">

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel">

                        <div class="panel-heading text-center">
                            <h4> Booking</h4>


                        </div>
                        <div class="panel-body">



                        </div>
                    </div>
                </div>
            </div>

            <hr>

        </div> <!-- /container -->


    </main>

    <?php include('footer.php'); ?>

</div>


<!-- Bootstrap core JavaScript

================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../../../../assets/js/vendor/popper.min.js"></script>
<script src="../../../../dist/js/bootstrap.min.js"></script> -->
</body>
</html>

